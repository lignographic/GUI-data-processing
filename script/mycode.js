// ================================================================ //
// ================ GUI grab data from a cross domain ============= //
// ================================================================ //


// ================================================================ //
// ======== exemple of a web site that allow CORS requests :
// https://jsonplaceholder.typicode.com



// ======== GUI LOGIC ======== //

window.onload = function(){

    var inputRoot = document.getElementById('inputRoot');
	var btn = document.getElementById('processData');
    var container = document.getElementById('container');
    var rowData;
    var commonLetter = document.getElementById('commonLetter');
    var theLongestWord = document.getElementById('theLongestWord');
    var wordsFrequency = document.getElementById('wordsFrequency');
    var aside = document.getElementById('aside');

    // create elm on the fly
    var containElm = document.createElement('div');
    var containElmText = document.createElement('p');
    var containElmTextNode = document.createTextNode('here is the frequency for each words : ');
    containElmText.appendChild(containElmTextNode);
    containElm.appendChild(containElmText);


    /// ===== create a CORS request ===== ///
    function createCorsRequest(method, root){
        var xhr = new XMLHttpRequest();

        if("withCredentials" in xhr){
            xhr.open(method, root, true);
        } else if(typeof XDomainRequest != "undefined"){
            xhr = XDomainRequest();
            xhr.open(method, url);
        } else {
            xhr = null;
        }
        return xhr;
    }

    /// ===== fire the CORS request ===== ///

    function launchCorsRequest(){
        var request = createCorsRequest('GET', inputRoot.value);

        if(!request){
            alert('CORS not supported');
            return;
        }

        request.onload = function(){
            //console.log(this.getResponseHeader('Content-type'));
            var grabbedData = this.responseText;
            rowData = grabbedData;
            extractContent(rowData);
        };
        request.onerror = function(){
            console.log('request failed, may be because the server that you are calling is not set to allow CORS request');
        };
        request.send();
    }



// ===== FUNCTIONS FOR EXTRACT DATA FROM URL AND PROCESS DATA ===== ///


// ===== extract content from get response ===== ///

function extractContent(data){
    container.innerHTML = data;
    var scripts = document.getElementsByTagName('script');
    var i = scripts.length;
    while(i--){
        scripts[i].parentNode.removeChild(scripts[i]);
    }
    var imgs = document.getElementsByTagName('img');
        var j = imgs.length;
        while(j--){
            imgs[j].parentNode.removeChild(imgs[j]);
        }
    var extracted = (container.textContent || container.innerText || '');
    container.innerHTML = 'text content extracted from ' + inputRoot.value + ' : ' + '\n' + extracted;
    mostCommonLetter(extracted);
    longestWord(extracted);
    var freq = eachWordsFrequency(extracted);
    Object.keys(freq).sort().forEach(function(word) {
        var elm = document.createElement('p');
        elm.innerHTML = "the word '" + word + "' is repeted " + freq[word] + ' time';
        containElm.appendChild(elm);
    });
}



// ===== FUNCTIONS FOR PERFORM STATISTICS ===== ///

// ===== Most common letter ===== ///

    function mostCommonLetter(string){
    var letters = string.replace(/\s/g,'');
    var lettersCounts = {};
    var maxKey = '';
    for(var i = 0; i < letters.length; i++) {
        var key = letters[i];
        if(!lettersCounts[key]){
         lettersCounts[key] = 0;
        }
        lettersCounts[key]++;
        if(maxKey == '' || lettersCounts[key] > lettersCounts[maxKey]){
            maxKey = key;
        }
    }
    var mostCommun = maxKey + ' ( repeted n°' + lettersCounts[maxKey] + ' time)';
    commonLetter.innerHTML = 'the most common letter is : ' + mostCommun;
}


// ===== each Words Frequency ===== ///

    function eachWordsFrequency(string) {
      var count = {};
      string.split(' ').forEach(function(s) {
         count[s] ? count[s]++ : count[s] = 1;
      });
      wordsFrequency.appendChild(containElm);
      return count;
    }


// ===== longest Word ===== ///

    function longestWord(string){
        var strWithoutLineBreak = string.replace(/\r?\n|\r/g, ' ');
        var str = strWithoutLineBreak.split(' ');
        var longest = 0;
        var word = '';

        for(var k = 0; k < str.length; k++){
            if(longest < str[k].length){
                longest = str[k].length;
                word = str[k];
            }
        }
        theLongestWord.innerHTML = 'the longest word is : \'' + word + '\' with ' + longest + ' letters';
    }



// ================================================================ //
// ===== FIRE A GET REQUEST AFTER ENTER A URL INTO THE URL FIELD ===== ///

// ===== call the launchCorsRequest function ===== ///

	btn.addEventListener('click', function(){
		launchCorsRequest();
	});


}	// end window.onload function