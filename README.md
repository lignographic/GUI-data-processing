## Synopsis

XHR 1 can’t operate a cross-domain request because the same-origin security policy, so we had
to find another solution.

Using YQL by Yahoo! is to ‘tricky’ so i have finally chosen CORS (Cross-origin resource sharing)
as a robust standard supported and normalized by the W3C.

## Techno used

Less pre-processor
CSS Media Queries
Ajax / XHR 2 / XDR / CORS
running locally on a nodeJS http-server	(npm)

## Targeted devices

targeted device screen sizes :  from 320px width to 1920px width.